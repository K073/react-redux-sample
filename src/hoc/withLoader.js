import React, {Fragment} from 'react';
import Loader from "../components/Loader/Loader";

const withLoader = (WrappedComponent, axios) => {
  return class extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        loaded: null
      };
      axios.interceptors.request.use((req) => {
        this.setState({loaded: true});
        return req
      });

      axios.interceptors.response.use((res) => {
        this.setState({loaded: false});
        return res
      });
    }
    render() {
      return <Fragment>
        {this.state.loaded ? <Loader/> : null}
        <WrappedComponent {...this.props}/>
      </Fragment>
    }
  }
};

export default withLoader;
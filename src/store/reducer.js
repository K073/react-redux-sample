import {INCREMENT, DECREMENT, ADD, SUBTRACT, FETCH_COUNTER_SUCCESS} from "./actions-counter";
import {CREATE_TODO, DELETE_TODO, FETCH_TODO_SUCCESS, SPLICE_TO_COMPLETE, SPLICE_TO_UNDERWAY} from "./actions-todo";

const initialState = {
  counter: 0,
  activeToDo: [],
  underwayToDo: [],
  completeToDo: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case INCREMENT:
      return {...state, counter: state.counter + 1};
    case DECREMENT:
      return {...state, counter: state.counter - 1};
    case ADD:
      return {...state, counter: state.counter + action.amount};
    case SUBTRACT:
      return {...state, counter: state.counter - action.amount};
    case FETCH_COUNTER_SUCCESS:
      return {...state, counter: state.counter = action.counter};
    case CREATE_TODO:
      const activeToDo = [...state.activeToDo];
      activeToDo.push(action.string);
      return {...state, activeToDo};
    case SPLICE_TO_UNDERWAY:
      const underwayToDo = [...state.underwayToDo];
      underwayToDo.push(state.activeToDo.splice(action.index, 1)[0]);
      return {...state, underwayToDo};
    case SPLICE_TO_COMPLETE:
      const completeToDo = [...state.completeToDo];
      completeToDo.push(state.underwayToDo.splice(action.index, 1)[0]);
      return {...state, completeToDo};
    case DELETE_TODO:
      const completeToDoRemoved = [...state.completeToDo];
      completeToDoRemoved.splice(action.index, 1);
      return {...state, completeToDo: completeToDoRemoved};
    case FETCH_TODO_SUCCESS:
      if (action.todo) {
        const fetchActiveToDo = action.todo.todoList.activeToDo || [];
        const fetchUnderwayToDo = action.todo.todoList.underwayToDo || [];
        const fetchCompleteToDo = action.todo.todoList.completeToDo || [];
        return {
          ...state,
          activeToDo: fetchActiveToDo,
          underwayToDo: fetchUnderwayToDo,
          completeToDo: fetchCompleteToDo
        }
      }
      return state;
    default:
      return state;
  }
};



export default reducer;

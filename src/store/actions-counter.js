import axios from '../axios-counter';

export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';

export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_ERROR = 'FETCH_COUNTER_ERROR';

export const SEND_COUNTER_REQUEST = 'SEND_COUNTER_REQUEST';
export const SEND_COUNTER_SUCCESS = 'SEND_COUNTER_SUCCESS';
export const SEND_COUNTER_ERROR = 'SEND_COUNTER_ERROR';

export const incrementCounter = () => {
  return { type: INCREMENT };
};

export const decrementCounter = () => {
  return { type: DECREMENT };
};

export const addCounter = (amount) => {
  return { type: ADD, amount};
};

export const subtractCounter = (amount) => {
  return { type: SUBTRACT, amount};
};

export const fetchCounterRequest = () => {
  return { type: FETCH_COUNTER_REQUEST };
};

export const fetchCounterSuccess = (counter) => {
  return { type: FETCH_COUNTER_SUCCESS, counter};
};

export const fetchCounterError = () => {
  return { type: FETCH_COUNTER_ERROR };
};

export const fetchCounter = () => {
  return dispatch => {
    dispatch(fetchCounterRequest());
    axios.get('/.json').then(response => {
      dispatch(fetchCounterSuccess(response.data));
    }, error => {
      dispatch(fetchCounterError());
    });
  }
};

export const sendCounterRequest = () => {
  return { type: SEND_COUNTER_REQUEST };
};

export const sendCounterSuccess = () => {
  return { type: SEND_COUNTER_SUCCESS};
};

export const sendCounterError = () => {
  return { type: SEND_COUNTER_ERROR };
};

export const sendCounter = value => {
  return dispatch => {
    dispatch(sendCounterRequest());
    axios.put('/.json', value).then(res => {
      dispatch(sendCounterSuccess());
    }, error => {
      dispatch(sendCounterError());
    })
  }
};
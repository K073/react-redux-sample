import axios from '../axios-todo';

export const CREATE_TODO = 'CREATE_TODO';
export const SPLICE_TO_UNDERWAY = 'SPLICE_TO_UNDERWAY';
export const SPLICE_TO_COMPLETE = 'SPLICE_TO_COMPLETE';
export const DELETE_TODO = 'DELETE_TODO';

export const FETCH_TODO_REQUEST = 'FETCH_TODO_REQUEST';
export const FETCH_TODO_SUCCESS = 'FETCH_TODO_SUCCESS';
export const FETCH_TODO_ERROR = 'FETCH_TODO_ERROR';

export const SEND_TODO_REQUEST = 'SEND_TODO_REQUEST';
export const SEND_TODO_SUCCESS = 'SEND_TODO_SUCCESS';
export const SEND_TODO_ERROR = 'SEND_TODO_ERROR';

export const createTodo = string => {
  return {type: CREATE_TODO, string}
};

export const spliceToUnderway = index => {
  return {type: SPLICE_TO_UNDERWAY, index}
};

export const spliceToComplete = index => {
  return {type: SPLICE_TO_COMPLETE, index}
};

export const deleteTodo = index => {
  return {type: DELETE_TODO, index}
};

export const fetchTodoRequest = () => {
  return { type: FETCH_TODO_REQUEST };
};

export const fetchTodoSuccess = (todo) => {
  return { type: FETCH_TODO_SUCCESS, todo};
};

export const fetchTodoError = () => {
  return { type: FETCH_TODO_ERROR };
};

export const fetchTodo = () => {
  return dispatch => {
    dispatch(fetchTodoRequest());
    axios.get('/.json').then(response => {
      dispatch(fetchTodoSuccess(response.data));
    }, error => {
      dispatch(fetchTodoError());
    });
  }
};

export const sendTodoRequest = () => {
  return { type: SEND_TODO_REQUEST };
};

export const sendTodoSuccess = () => {
  return { type: SEND_TODO_SUCCESS};
};

export const sendTodoError = () => {
  return { type: SEND_TODO_ERROR };
};

export const sendTodo = todoList => {
  return dispatch => {
    dispatch(sendTodoRequest());
    axios.put('/.json', {todoList: todoList}).then(res => {
      dispatch(sendTodoSuccess());
    }, error => {
      dispatch(sendTodoError());
    })
  }
};
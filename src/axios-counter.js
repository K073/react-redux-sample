import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://home-work68.firebaseio.com/counter/'
});

export default instance;

import React from 'react';
import loader from '../../preloader.gif';

const Loader = props => {
  return <div style={{
    position: 'fixed',
    top: '0',
    left: '0',
    bottom: '0',
    right: '0',
    background: `#fff url(${loader}) no-repeat center`
  }}/>
};

export default Loader;
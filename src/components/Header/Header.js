import React from 'react';
import { Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const Header = props => (
  <Navbar>
    <Navbar.Header>
      <Navbar.Brand>
        <LinkContainer to="/" exact>
          <a>Home Work 68</a>
        </LinkContainer>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav pullRight>
        <LinkContainer to="/" exact>
          <NavItem>Counter</NavItem>
        </LinkContainer>
        <LinkContainer to="/todo" exact>
          <NavItem>TODO</NavItem>
        </LinkContainer>
        <LinkContainer to="/about" exact>
          <NavItem>About</NavItem>
        </LinkContainer>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);

export default Header;
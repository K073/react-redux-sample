import React from 'react';
import {Button, Col, FormControl, Grid, Panel, Row} from "react-bootstrap";
import {connect} from "react-redux";
import {
  createTodo, deleteTodo, fetchTodo, sendTodo, spliceToComplete,
  spliceToUnderway
} from "../../store/actions-todo";

const mapStateToProps = state => {
  return {
    activeToDo: state.activeToDo,
    underwayToDo: state.underwayToDo,
    completeToDo: state.completeToDo
  };
};

const mapDispatchToProps = dispatch => {
  return {
    createTodo: value => dispatch(createTodo(value)),
    spliceToUnderway: id => dispatch(spliceToUnderway(id)),
    spliceToComplete: id => dispatch(spliceToComplete(id)),
    deleteTodo: id => dispatch(deleteTodo(id)),
    fetchTodo: () => dispatch(fetchTodo()),
    sendTodo: todoList => dispatch(sendTodo(todoList))
  }
};

class Todo extends React.Component {
  state = {
    createToDoValue: ''
  };

  createToDoHandler = event => {
    this.setState({createToDoValue: event.target.value})
  };

  componentDidUpdate() {
    this.props.sendTodo({
      activeToDo: this.props.activeToDo,
      underwayToDo: this.props.underwayToDo,
      completeToDo: this.props.completeToDo
    })
  }

  createToDo = event => {
    event.preventDefault();
    this.props.createTodo(this.state.createToDoValue)
  };

  componentDidMount() {
    this.props.fetchTodo()
  }
  render() {
    return (
      <div className={'container'}>
        <Grid>
          <Row style={{marginBottom: '20px'}}>
            <form onSubmit={this.createToDo}>
              <Col md={10}>
                <FormControl
                  type="text"
                  placeholder="Create TODO"
                  value={this.state.createToDoValue}
                  onChange={this.createToDoHandler}
                />
              </Col>
              <Col md={2}>
                <Button type={'submit'} className={'pull-right'}>Create</Button>
              </Col>
            </form>
          </Row>
          <Row>
            <Col md={4}>
              {this.props.activeToDo.map((value, index) =>
                <Panel key={index} bsStyle="warning">
                  <Panel.Heading>
                    <Panel.Title componentClass="h3">Active</Panel.Title>
                  </Panel.Heading>
                  <Panel.Body>{value}</Panel.Body>
                  <Panel.Footer className={'clearfix'}><Button onClick={() => this.props.spliceToUnderway(index)} className={'pull-right'}>Next</Button></Panel.Footer>
                </Panel>)}
            </Col>
            <Col md={4}>
              {this.props.underwayToDo.map((value, index) =>
                <Panel key={index} bsStyle="primary">
                  <Panel.Heading>
                    <Panel.Title componentClass="h3">To Do</Panel.Title>
                  </Panel.Heading>
                  <Panel.Body>{value}</Panel.Body>
                  <Panel.Footer className={'clearfix'}><Button onClick={() => this.props.spliceToComplete(index)} className={'pull-right'}>Next</Button></Panel.Footer>
                </Panel>)}
            </Col>
            <Col md={4}>
              {this.props.completeToDo.map((value, index) =>
                <Panel key={index} bsStyle="success">
                  <Panel.Heading>
                    <Panel.Title componentClass="h3">Completed</Panel.Title>
                  </Panel.Heading>
                  <Panel.Body>{value}</Panel.Body>
                  <Panel.Footer className={'clearfix'}><Button onClick={() => this.props.deleteTodo(index)} className={'pull-right'} bsStyle={'danger'}>Delete</Button></Panel.Footer>
                </Panel>)}
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Todo);
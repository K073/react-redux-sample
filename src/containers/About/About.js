import React from 'react';
import {PageHeader} from "react-bootstrap";
import axios from 'axios';
import withLoader from "../../hoc/withLoader";

class About extends React.Component {
  state = {
    content: ''
  };

  componentDidMount() {
    axios.get('https://home-work68.firebaseio.com/about.json').then(res => {
      this.setState({content: res.data})
    })
  }

  render() {
    return (
      <div className={'container'}>
        <PageHeader>
          About
        </PageHeader>
        <p>
          {this.state.content}
        </p>
      </div>
    );
  }
}

export default withLoader(About, axios);
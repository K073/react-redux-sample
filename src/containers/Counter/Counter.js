import React from 'react';
import {Button, Col, Grid, Row} from "react-bootstrap";
import {connect} from "react-redux";
import {
  addCounter, decrementCounter, fetchCounter, incrementCounter, sendCounter,
  subtractCounter
} from "../../store/actions-counter";

const mapStateToProps = state => {
  return {
    counter: state.counter
  };
};

const mapDispatchToProps = dispatch => {
  return {
    increaseCounter: () => dispatch(incrementCounter()),
    decreaseCounter: () => dispatch(decrementCounter()),
    addCounter: () => dispatch(addCounter(5)),
    subtractCounter: () => dispatch(subtractCounter(5)),
    fetchCounter: () => dispatch(fetchCounter()),
    sendCounter: value => dispatch(sendCounter(value))
    }
};

class Counter extends React.Component {
  componentDidMount() {
    this.props.fetchCounter();
  }

  componentDidUpdate() {
    this.props.sendCounter(this.props.counter);
  }

  render() {
    return (
      <div className={'container'}>
        <Grid>
          <Row>
            <Col className={'text-center'} md={6} mdOffset={3}>
              <h1>{this.props.counter}</h1>
              <Button onClick={this.props.increaseCounter}>Increase</Button>
              <Button onClick={this.props.decreaseCounter}>Decrease</Button>
              <Button onClick={this.props.addCounter}>Increase by 5</Button>
              <Button onClick={this.props.subtractCounter}>Decrease by 5</Button>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);
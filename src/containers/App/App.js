import React, { Component } from 'react';
import Header from "../../components/Header/Header";
import {Switch} from "react-router-dom";
import Route from "react-router/es/Route";
import Counter from "../Counter/Counter";
import Todo from "../Todo/Todo";
import About from "../About/About";

class App extends Component {
  render() {
    return (
      <div>
        <Header/>
        <Switch>
          <Route path={'/'} exact component={Counter}/>
          <Route path={'/todo'} exact component={Todo}/>
          <Route path={'/about'} exact component={About}/>
        </Switch>
      </div>
    );
  }
}

export default App;
